package com.example.karina_1202160041_si4001_pab_modul5;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.Holder> {
    List<Article> articles;

    public Adapter(List<Article> articles) {
        this.articles = articles;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflate = LayoutInflater.from(viewGroup.getContext());
        View view = inflate.inflate(R.layout.article_list, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        holder.bind(articles.get(i));
    }

    @Override
    public int getItemCount() {
        return articles == null ? 0 : articles.size();
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView dAuthor, dTitle, article;
        private int idArtikel;

        public Holder(@NonNull View itemView) {
            super(itemView);
            dAuthor = itemView.findViewById(R.id.dAuthor);
            dTitle = itemView.findViewById(R.id.dTitle);
            article = itemView.findViewById(R.id.dArticle);

            itemView.setOnClickListener(this);
        }

        void bind(Article art) {
            dAuthor.setText(art.getAuthor());
            dTitle.setText(art.getTitle());
            article.setText(art.getDescription());
            idArtikel = art.getId();

        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(itemView.getContext(), DetailArticle.class);
            intent.putExtra("id", idArtikel);
            itemView.getContext().startActivity(intent);
        }
    }
}
