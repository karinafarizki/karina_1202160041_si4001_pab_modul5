package com.example.karina_1202160041_si4001_pab_modul5;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button listArtikel, create, setting;
    TextView txtMain;
    private Context ctx;
    public static final int RC_SETTING = 144;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPrefManager pref= new SharedPrefManager(this);
        if (pref.isNightMode()) {
            setTheme(R.style.AppTheme_Dark);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Article Apps");
        ctx = this;
        txtMain = findViewById(R.id.txtMain);
        listArtikel = findViewById(R.id.btnList);
        create = findViewById(R.id.btnCreate);
        setting = findViewById(R.id.btnSetting);

        if (pref.isNightMode()) {
            txtMain.setText("Good Night");
        }
        listArtikel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ctx, ListArticle.class));
            }
        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ctx, CreateArticle.class));
            }
        });

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(ctx, Setting.class), RC_SETTING);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == RC_SETTING) {
            recreate();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
