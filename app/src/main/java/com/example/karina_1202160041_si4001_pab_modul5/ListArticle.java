package com.example.karina_1202160041_si4001_pab_modul5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

public class ListArticle extends AppCompatActivity {

    RecyclerView recyclerView;
    Adapter adapter;
    Helper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPrefManager pref= new SharedPrefManager(this);
        if (pref.isNightMode()) {
            setTheme(R.style.AppTheme_Dark);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_article);
        getSupportActionBar().setTitle("Article Apps");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView = findViewById(R.id.idRecyclerView);
        db = new Helper(this);
        adapter = new Adapter(db.getAll());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
