package com.example.karina_1202160041_si4001_pab_modul5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;

public class Setting extends AppCompatActivity {

    Switch swMode, swFont;
    Button btnSave;
    SharedPrefManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        pref = new SharedPrefManager(this);
        if (pref.isNightMode()) {
            setTheme(R.style.AppTheme_Dark);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        getSupportActionBar().setTitle("Article Apps");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        btnSave = findViewById(R.id.btnSave);
        swMode = findViewById(R.id.swMode);
        swFont = findViewById(R.id.swFont);
        if (pref.isNightMode()) {
            swMode.setChecked(true);
        }

        if (pref.isBigFont()) {
            swFont.setChecked(true);
        }
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (swMode.isChecked()) {
                    pref.setNightMode(true);
                } else {
                    pref.setNightMode(false);
                }

                if (swFont.isChecked()) {
                    pref.setBigFont(true);
                } else {
                    pref.setBigFont(false);
                }
                recreate();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
