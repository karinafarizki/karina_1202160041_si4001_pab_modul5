package com.example.karina_1202160041_si4001_pab_modul5;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CreateArticle extends AppCompatActivity {

    EditText title, author, desc;
    Button btnPost;
    private Helper db;
    private Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPrefManager pref= new SharedPrefManager(this);
        if (pref.isNightMode()) {
            setTheme(R.style.AppTheme_Dark);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_article);
        getSupportActionBar().setTitle("Create Post");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ctx = this;
        title = findViewById(R.id.inputTitle);
        author = findViewById(R.id.inputAuthor);
        desc = findViewById(R.id.inputArticle);
        btnPost = findViewById(R.id.btnPost);
        db = new Helper(this);
        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (title.getText().toString().isEmpty() || author.getText().toString().isEmpty()
                        || desc.getText().toString().isEmpty()) {
                    Toast.makeText(ctx, "Field Wajib Diisi", Toast.LENGTH_SHORT).show();
                    return;
                }
                SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yy");
                Date date = new Date();
                db.insert(new Article(title.getText().toString(),
                        author.getText().toString(), desc.getText().toString(), sdf.format(date)));
                Toast.makeText(ctx, "Berhasil Post Artikel", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
