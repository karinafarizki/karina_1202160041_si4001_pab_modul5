package com.example.karina_1202160041_si4001_pab_modul5;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.List;

public class Helper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "article.db";

    private static final String ARTICLE_TABLE_CREATE =
            "CREATE TABLE " + ArticleEntry.TABLE_NAME + " (" +
                    ArticleEntry._ID + " INTEGER PRIMARY KEY," +
                    ArticleEntry.COLUMN_NAME_TITLE + " TEXT," +
                    ArticleEntry.COLUMN_NAME_DESC + " TEXT," +
                    ArticleEntry.COLUMN_NAME_AUTHOR + " TEXT," +
                    ArticleEntry.COLUMN_NAME_CREATED + " DATE )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + ArticleEntry.TABLE_NAME;

    public Helper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(ARTICLE_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DELETE_ENTRIES);
        onCreate(sqLiteDatabase);

    }

    public void insert(Article article) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(ArticleEntry.COLUMN_NAME_TITLE, article.getTitle());
        values.put(ArticleEntry.COLUMN_NAME_AUTHOR, article.getAuthor());
        values.put(ArticleEntry.COLUMN_NAME_DESC, article.getDescription());
        values.put(ArticleEntry.COLUMN_NAME_CREATED, article.getCreated_at());
        db.insert(ArticleEntry.TABLE_NAME, null, values);
        db.close();

    }

    public Article getSingle(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] projection = {
                ArticleEntry._ID,
                ArticleEntry.COLUMN_NAME_TITLE,
                ArticleEntry.COLUMN_NAME_AUTHOR,
                ArticleEntry.COLUMN_NAME_DESC,
                ArticleEntry.COLUMN_NAME_CREATED

        };
        String[] selectionArgs = {String.valueOf(id)};
        Cursor cursor = db.query(ArticleEntry.TABLE_NAME,
                projection,
                ArticleEntry._ID + "=?",
                selectionArgs,
                null,
                null,
                null,
                null);

        if (cursor != null)
            cursor.moveToFirst();
        return new Article(
                cursor.getInt(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4)
        );
    }

    public List<Article> getAll() {
        List<Article> articleList = new ArrayList<>();

        String query = "SELECT * FROM " + ArticleEntry.TABLE_NAME;

        Cursor cursor = getWritableDatabase().rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Article article = new Article(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4)
                );
                articleList.add(article);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return articleList;
    }

    public final class ArticleEntry implements BaseColumns {
        static final String TABLE_NAME = "article";
        static final String COLUMN_NAME_TITLE = "title";
        static final String COLUMN_NAME_AUTHOR = "author";
        static final String COLUMN_NAME_DESC = "description";
        static final String COLUMN_NAME_CREATED = "created_at";
    }
}


