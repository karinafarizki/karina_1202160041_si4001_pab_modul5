package com.example.karina_1202160041_si4001_pab_modul5;

public class Article {
    private int id;
    private String title;
    private String author;
    private String description;
    private String created_at;

    public Article (String title, String author, String description, String created_at) {
        this.title = title;
        this.author = author;
        this.description = description;
        this.created_at = created_at;
    }

    public Article(int id, String title, String author, String description, String created_at) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.description = description;
        this.created_at = created_at;
    }

    public int getId() {
        return id;
    }

    public String getTitle(){
        return title;
    }

    public String getAuthor(){
        return author;
    }

    public String getDescription() {
        return description;
    }

    public String getCreated_at() {
        return created_at;
    }

}
