package com.example.karina_1202160041_si4001_pab_modul5;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefManager {

    public static final String ARTICLE_APP = "article_apps";

    public static final String IS_NIGHT_MODE = "night_mode";
    public static final String IS_BIG_FONT = "big_font";


    SharedPreferences sp;

    public SharedPrefManager(Context context) {
        sp = context.getSharedPreferences(ARTICLE_APP, Context.MODE_PRIVATE);
    }

    public void setNightMode(Boolean status) {
        sp.edit().putBoolean(IS_NIGHT_MODE, status).apply();
    }

    public void setBigFont(Boolean status) {
        sp.edit().putBoolean(IS_BIG_FONT, status).apply();
    }

    public boolean isNightMode() {
        return sp.getBoolean(IS_NIGHT_MODE, false);
    }

    public boolean isBigFont() {
        return sp.getBoolean(IS_BIG_FONT, false);
    }
}
