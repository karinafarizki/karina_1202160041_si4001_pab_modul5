package com.example.karina_1202160041_si4001_pab_modul5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.widget.TextView;
import android.widget.Toast;

public class DetailArticle extends AppCompatActivity {

    TextView txtTitle, txtAuthor, txtDate, txtArticle;
    private Helper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPrefManager pref= new SharedPrefManager(this);
        if (pref.isNightMode()) {
            setTheme(R.style.AppTheme_Dark);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_article);
        getSupportActionBar().setTitle("Article Apps");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txtTitle = findViewById(R.id.txtTitle);
        txtAuthor = findViewById(R.id.txtAuthor);
        txtDate = findViewById(R.id.txtDate);
        txtArticle = findViewById(R.id.txtArticle);
        if (pref.isBigFont()) {
            txtArticle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
        }
        db = new Helper(this);
        int idArtikel = getIntent().getIntExtra("id", 0);
        getArtikel(db.getSingle(idArtikel));
    }

    void getArtikel(Article article) {
        txtTitle.setText(article.getTitle());
        txtAuthor.setText(article.getAuthor());
        txtDate.setText(article.getCreated_at());
        txtArticle.setText(article.getDescription());

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
